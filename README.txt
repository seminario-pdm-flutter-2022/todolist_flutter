Aplicativo de lista de taerfas, desenvolvido em Flutter, que permite a adição de tarefas, marcar se a mesma foi concluída.
Também permite ordenar a lista ao arrastar pra baixo e atualizar, excluir uma tarefa e desfazer a ação de exclusão dentro de alguns segundos.

Autores: Guilherme Rodrigues de Melo e Maria Luísa de Carvalho Mardegan.

Projeto baseado em um curso de Flutter em plataforma de cursos online.